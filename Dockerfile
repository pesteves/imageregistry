FROM gitlab-registry.cern.ch/swan/docker-images/systemuser:v5.18.7

COPY ./jupyter-extensions .

RUN pip install ./SwanContents
